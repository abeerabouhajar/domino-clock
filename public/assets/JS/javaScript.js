canvas = document.getElementById("the_canvas");
context = canvas.getContext("2d");  
canvas.width = 1000;
canvas.height = 450;

let hours = new Image();
let hourLeft = new Image();
let minTens = new Image();
let min = new Image();
let dots = new Image();
let dotsWhite = new Image();

let hourX = 250;
let hourY = 150;


console.log("fill rect OUYT");

// hours.onload = function () 
// {
//     fillRect();
// }


function fillRect() 
{
    
    
   console.log("fill rect");
    let date = new Date(); 
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    let hhIn12 = (hh -12);
   

    if (hh < 13){
        hourLeft.src = "assets/Img/"+ Math.floor(hh /10) +"d.png";
        hours.src = "assets/Img/"+ Math.floor(hh %10) +"d.png";
        minTens.src = "assets/Img/"+ Math.floor(mm/10) +"d.png";
        min.src = "assets/Img/"+ (mm - (Math.floor(mm/10)*10)) +"d.png";    
        dots.src="assets/Img/dots.wh.png";

        window.addEventListener("load",function() {
            changeBackground("white") });
       
    } else {
    hourLeft.src = "assets/Img/"+ Math.floor(hhIn12 /10) +"n.png";
    hours.src = "assets/Img/"+ Math.floor(hhIn12%10) +"n.png";
    minTens.src = "assets/Img/"+ Math.floor(mm/10) +"n.png";
    min.src = "assets/Img/"+ (mm - (Math.floor(mm/10)*10)) +"n.png";
    dots.src="assets/Img/middle.png";
    // changeBackground("white")
    window.addEventListener("load",function() {
        changeBackground("black") });
   
    }

    if (window.matchMedia('screen and (max-width: 395px)').matches) {
        console.log("SMALL SCREEN")
        canvas.width = 200;
        canvas.height = 381;
        context.clearRect(0, 0, canvas.width, canvas.height);
        var sizeUP = 153/4;
        var sizeDown = 293/4;
        
        context.drawImage(hours, hourX-110, hourY-80, sizeUP, sizeDown);
        context.drawImage(hourLeft, hourX-200, hourY-80, sizeUP, sizeDown);
        context.drawImage(minTens, hourX-110, hourY+80, sizeUP, sizeDown);
        context.drawImage(min, hourX-200, hourY+80, sizeUP, sizeDown);
        context.drawImage(dots,hourX-150, hourY+30,42/6, 82/6);
        context.drawImage(dotsWhite,hourX+240, hourY+130,42/6, 82/6);     

        
    } else {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.drawImage(hours, hourX, hourY, 153, 293);
        context.drawImage(hourLeft, hourX-180, hourY, 153, 293);
        context.drawImage(minTens, hourX+360, hourY, 153, 293);
        context.drawImage(min, hourX+538, hourY, 153, 293);
        context.drawImage(dots,hourX+240, hourY+130,42, 82);
        context.drawImage(dotsWhite,hourX+240, hourY+130,42, 82);     
    }

   


}


function changeBackground(color) {
    document.body.style.background = color;
    document.canvas.style.background = color;

 }
 
setInterval(fillRect, 0000);


